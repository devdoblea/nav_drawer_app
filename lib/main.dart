import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      title: 'Drawer',
      theme: ThemeData(
        primarySwatch: Colors.teal,
        fontFamily: 'Exo2',
      ),
      home: HomeScreen(),
    ));

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 30.0,
              child: Container(
                alignment: Alignment.bottomLeft,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: <Color>[
                      Colors.deepPurple,
                      Colors.deepPurpleAccent,
                    ],
                  ),
                ),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.close),
                  color: Colors.white,
                ),
              ),
            ),
            DrawerHeader(
              margin: const EdgeInsets.only(bottom: 8.0),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Colors.deepPurple,
                    Colors.deepPurpleAccent,
                  ],
                ),
              ),
              child: Container(
                child: Column(children: <Widget>[
                  Material(
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset(
                        'assets/images/buo-logo.png',
                        width:75.0, 
                        height: 75.0,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: Text(
                      'Drawer Flutter',
                      style: TextStyle(color: Colors.white, fontSize: 24.0),
                    )
                  ),
                ]),
              ),
            ),
            CustomListTile(Icons.person, 'Perfil', () => {}),
            CustomListTile(Icons.notifications, 'Notificaciones', () => {}),
            CustomListTile(Icons.settings, 'Configuracion', () => {}),
            CustomListTile(Icons.lock, 'Salir', () => {}),
          ]
        ),
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget> [
              Text(
                'Haz Tap en el Icono de la esquina y se abrirá el Navigation Drawer', 
                style: TextStyle(
                  fontSize: 26.0,
                ),
                textAlign: TextAlign.center,
              ),
            ]
          ),
        ),
      ),
    );
  }
}

class CustomListTile extends StatelessWidget {
  
  final IconData icon;
  final String texto;
  final Function onTap;

  CustomListTile(this.icon, this.texto, this.onTap);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.grey.shade400,
            ),
          ),
        ),
        child: InkWell(
          splashColor: Colors.deepPurple,
          onTap: onTap,
          child: Container(
            height: 40.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(icon),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        texto,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),                    
                  ],
                ),
                Icon(Icons.arrow_right),
              ]
            ),
          ),
        )
      ),
    );
  }
}
