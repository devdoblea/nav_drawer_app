Barra lateral haciendo uso del Widget Drawer.
Tambien le agrego un botón "Cerrar" porque de primera mano
el usuario no sabrìa como cerrar el Navigation Drawer aunque
por defecto, la forma de cerrarlo es simplemente haciendo
"Swipe" desde el borde derecho del Navigation Drawer hacia la izquierda
 
De aqui extraje el ejemplo.

https://www.youtube.com/watch?v=jDQQM1RfjNc


Asi quedó:

![](assets/Screenshot_2019-12-01-13-27-44.png)
![](assets/Screenshot_2019-12-01-13-27-50.png)

# nav_drawer_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
